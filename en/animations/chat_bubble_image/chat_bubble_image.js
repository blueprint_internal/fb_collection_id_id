
function Chat_bubble_image(resources)
{
	Chat_bubble_image.resources = resources;
}
Chat_bubble_image.prototype = {
	init: function()
	{
		this.game = new Phaser.Game(870,700, Phaser.CANVAS, 'chat_bubble_image', { preload: this.preload, create: this.create, update: this.update, render: 
		this.render,parent:this },null,null,false);
	},

	preload: function()
	{
		
		this.game.scale.maxWidth = 870;
    	this.game.scale.maxHeight = 700;
		this.game.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;
		
		this.game.load.image('tiny_arrow', Chat_bubble_image.resources.tiny_arrow);
        this.game.load.image('phone_slice', Chat_bubble_image.resources.phone_slice);
		
		
		this.game.created = false;
    	
    	
    	
  
    	this.game.stage.backgroundColor = '#ffffff';
    	//0f2747
    	
	},

	create: function(evt)
	{
		
		if(this.game.created === false)
		{
			
			this.parent.placeholder = this.game.make.sprite(0,0,'placeholder');
			this.parent.placeholder.anchor.set(0.5,0.5);
            this.parent.game.stage.backgroundColor = '#ffffff';
			this.game.created  = true;
	    	this.parent.buildAnimation();
	    }
	},
    
	buildAnimation: function()
	{
		var style = Chat_bubble_image.resources.textStyle_1;
        //this.placeholder= this.game.add.sprite(this.game.world.centerX,this.game.world.centerY,'placeholder');
        //this.placeholder.anchor.set(0.5,0.5);
        this.phoneSlice = this.game.add.sprite(this.game.world.centerX+50,this.game.world.centerY,'phone_slice');
        this.phoneSlice.alpha = 0;
        this.phoneSlice.anchor.set(0.5,0.5);
        var bubble_1 = this.game.add.sprite(0,0);
        Messenger_efx.bubbleDraw({game:this.game,direction:"bottomRightCut",message:Chat_bubble_image.resources.message_1,style:style,sprite:bubble_1});
        bubble_1.x = 90;
        bubble_1.y = 200;
        bubble_1.alpha = 0;
        //
        var bubble_2 = this.game.add.sprite(0,0); Messenger_efx.bubbleDraw({game:this.game,direction:"bottomRightCut",message:Chat_bubble_image.resources.message_2,style:style,sprite:bubble_2});
        bubble_2.x = 66;
        bubble_2.y = 358;
        bubble_2.alpha = 0;
        
        var bubble_3 = this.game.add.sprite(0,0); Messenger_efx.bubbleDraw({game:this.game,direction:"topLeftCut",message:Chat_bubble_image.resources.message_3,style:style,sprite:bubble_3});
        bubble_3.x = 680;
        bubble_3.y = 400;
        bubble_3.alpha = 0;
        
        //
        
        var tiny_arrow_1 = this.game.add.sprite(bubble_1.x+172,bubble_1.y+50,'tiny_arrow');
        tiny_arrow_1.alpha = 0;
        var tiny_arrow_2 = this.game.add.sprite(bubble_2.x+200,bubble_2.y+25,'tiny_arrow');
         tiny_arrow_2.alpha = 0;
        var tiny_arrow_3 = this.game.add.sprite(bubble_3.x+10,420,'tiny_arrow');
         tiny_arrow_3.alpha = 0;
        tiny_arrow_3.scale.setTo(-1,1);  
        
        var phoneSlice_anim = this.game.add.tween(this.phoneSlice).to( { alpha:1,y:this.phoneSlice.y},500,Phaser.Easing.Quadratic.Out);
        this.phoneSlice.y-=0;
        
        var bubble_1_An = this.game.add.tween(bubble_1).to( { alpha:1,x:bubble_1.x},500,Phaser.Easing.Quadratic.Out);
        bubble_1.x-=50;
       
        var arrow_1_anim = this.game.add.tween(tiny_arrow_1).to( { alpha:1},500,Phaser.Easing.Quadratic.Out);
        bubble_1.x-=50;
        
        var bubble_2_An = this.game.add.tween(bubble_2).to( { alpha:1,x:bubble_2.x},500,Phaser.Easing.Quadratic.Out);
        bubble_2.x-=50;
       
        var arrow_2_anim = this.game.add.tween(tiny_arrow_2).to( { alpha:2},500,Phaser.Easing.Quadratic.Out);
        bubble_2.x-=50;
        
        var bubble_3_An = this.game.add.tween(bubble_3).to( { alpha:1,x:bubble_3.x},500,Phaser.Easing.Quadratic.Out);
        bubble_3.x+=50;
       
        var arrow_3_anim = this.game.add.tween(tiny_arrow_3).to( { alpha:2},500,Phaser.Easing.Quadratic.Out);
        
        
         
        phoneSlice_anim.chain(bubble_1_An,arrow_1_anim,bubble_2_An,arrow_2_anim,bubble_3_An,arrow_3_anim);
        phoneSlice_anim.start();
		
    },
	inview: function()
	{
		
		
	},

	animate: function()
	{
		//console.log("animate")
	},

	update: function()
	{

	},
	render: function()
	{
		//this.game.debug.inputInfo(32, 32);
	}

}
function Messenger_efx()
{
}

//config {direction:left,message:"text go here\nbreaking",style:style}
Messenger_efx.bubbleDraw = function(config)
{
    var padding = 10;
    var radius = 8;
    var bubble = config.sprite;
    var bubbleText = config.game.make.text(0,0,config.message,config.style);


    bubble.addChild(Messenger_efx.shapeBubble(bubbleText.width+(radius*4),bubbleText.height+(radius),config.direction,radius,bubble,config.game));
    bubble.addChild(bubbleText);
    var bubbleHeight = Number(bubbleText.height+radius)+padding;


    bubbleText.smoothed = true;
    bubble.smoothed = false;
    bubbleText.x = (radius*2)+10;
    bubbleText.y = radius+7;
    bubble.x = 40;
    if(config.direction == "left")
    {
        //bubbleText.fill = "#181818";
       // var head = config.game.make.sprite(0,bubbleText.height,'head');
        //bubble.addChild(head);
    }
    else
    {

        //bubble.x = 270-bubbleText.width;
        //bubbleText.fill = "#f8f8f8";
    }

    //config.sprite.add(bubble);



    /*
    var tw = this.game.add.tween(this.message_zone).to({y:this.message_zone.y-bubbleHeight-radius},350,Phaser.Easing.Quadratic.Out);

    tw.start();

    this.bubbleCount++;

    this.message_zone.smoothed = true;
    bubbleText.x = Math.round(bubbleText.x);
    return
    */
        
}
Messenger_efx.shapeBubble = function(width,height,direction,radius,parent,game)
{
            var corner = radius*2;
            var bubbleColor = 0x4267b2;
            var bubbleStroke = 0xffffff;
            var leftBottomCorner = corner;
            var rightBottomCorner = width 
            var rightTopCorner = corner+radius;
            switch(direction)
            {
                    /*
                case "left":
                    
                    //leftBottomCorner = corner+radius;
                break
                case "right":
                    //bubbleColor = 0x0284fe;
                     rightBottomCorner = width+radius

                break
                */
                case "topLeftCut":
                    rightTopCorner = corner;
                break
                case "bottomRightCut":
                    rightBottomCorner = width+radius;
                break

            }

            var graphics = game.add.graphics(0,0);
            graphics.lineStyle(2, bubbleStroke,2);
            graphics.beginFill(bubbleColor);
            graphics.moveTo(corner,corner);
            graphics.quadraticCurveTo(corner, radius,rightTopCorner,radius);
            graphics.lineTo(width,radius);
            graphics.quadraticCurveTo(width+radius, radius, width+radius,corner);
            graphics.lineTo(width+radius,height);
            graphics.quadraticCurveTo(width+radius, height+radius, rightBottomCorner,height+radius);
            graphics.lineTo(corner+radius,height+radius);
            graphics.quadraticCurveTo(corner,height+radius,corner,height);
            graphics.lineTo(corner,corner);

            return graphics
}

//course/en/animations/nielsen_audience/
/*
var _resources= 
{
    "textStyle_1":{"font":"15px freight-sans-pro","fill": "#ffffff","wordWrap": false,"wordWrapWidth":200,"align": "center","lineSpacing": -10 },
    "phone_slice":"/chat_bubble_image/phone_slice.png",
    "tiny_arrow":"/chat_bubble_image/tinyarrow.png",
    "message_1":"Proximity based ad copy",
    "message_2":"Proximity based CTA destination",
    "message_3":"Proximity based\nmap in carousel",
}
var start = new Chat_bubble_image(_resources).init();
*/


